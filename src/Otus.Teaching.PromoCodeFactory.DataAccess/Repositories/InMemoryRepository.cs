﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T entity)
        {
            var result = true;

            if (Data.Where(x => x.Id == entity.Id).Any())
                result = false;
            else
                Data = Data.Append(entity);
      
            return Task.FromResult(result);
        }

        public Task<bool> UpdateAsync(T entity, Action<T, T> action)
        {
            var result = true;

            var x = Data.FirstOrDefault(x => x.Id == entity.Id);

            if (x == null)
                result = false;
            else
            {
                action(x, entity);
            }

            return Task.FromResult(result);
        }

        public Task DeleteByIdAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();

            return Task.CompletedTask;
        }
    }
}